package com.chase.beginningspring5.annotation;

import org.jtwig.web.servlet.JtwigRenderer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/hello1")
public class FirstHelloServlet extends HttpServlet {
    /**
	 * 
	 */
	private static final long serialVersionUID = -4427011101374936594L;
	//private final JtwigRenderer renderer = JtwigRenderer.defaultRenderer();

	private HelloBean helloBean;
	private ApplicationContext ac;
	
	@Override
	public void init(ServletConfig config) throws ServletException {

		super.init();
		ApplicationContext ac = (ApplicationContext) config.getServletContext().getAttribute("applicationContext");
		helloBean = ac.getBean(HelloBean.class);
	}
	
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	/*
        renderer.dispatcherFor("/WEB-INF/templates/jtwig/hello.jtwig.html")
                .with("name", "world")
                .render(request, response);
                */
    	response.getWriter().write(helloBean.toString());
    }
}
