package com.chase.beginningspring5.annotation;

import org.springframework.stereotype.Component;

@Component
public class HelloBean {

	@Override
	public String toString() {
		
		return "Dependency Injection is configured properly";
	}
}
